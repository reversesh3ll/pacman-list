# Pacman List

Simple pacman hook for backing up your installed packages to your home directory.

Currently it saves two seperate lists. One for repo packages (.packages) and one for AUR packages (.packages-aur).
These files can then be backed up with git for redundancy.

**Place the file in your pacman hooks directory. eg. /etc/pacman.d/hooks/**

**IMPORTANT** - Make sure to change the **USERNAME** in the file to your username before use!